import languageService from '../services/languagesService';
import { fetchingAction } from "./fetchingAction";
import { hideLoading, showLoading } from "react-redux-loading-bar";

/** get locales **/
export const getAllLocalesActionThunk = () => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return languageService.getLocales()
                .then(result => {
                    // store locales
                    dispatch(storeLocalesAction(result));
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

export const storeLocalesAction = (locales) => {
    return {
        type: 'STORE_ALL_SITE_LOCALES',
        locales
    }
};

// ToDo: when logout
export const clearLocalesAction = () => {
    return {
        type: 'CLEAR_SITE_LOCALES',
        locales:null
    }
};

// ToDo: when user switched language
export const selectUserLocaleAction = (userLocale) => {
    return {
        type: 'SELECT_USER_LOCALE',
        userLocale
    }
};

// ToDo: when logout
export const resetUserLocaleAction = () => {
    return {
        type: 'RESET_USER_LOCALE',
        userLocale:null
    }
};