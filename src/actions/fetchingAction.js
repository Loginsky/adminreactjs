export const fetchingAction = (state) => {
    return {
        type: 'SET_FETCHING',
        payload: state
    }
};