export const notificationAction = (show = false, error = false, message = '') => {
    return {
        type: 'SET_NOTIFICATION',
        show,
        error,
        message
    }
};