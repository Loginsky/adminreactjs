import { showLoading, hideLoading } from 'react-redux-loading-bar';

import { fetchingAction } from './fetchingAction';
import authService from "../services/authService";

import RequestAPI from '../helpers/server.js';

export const logInAction = (email,password,remember) => {
    return (dispatch,getState) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        dispatch(requestLogin());
        return new Promise((resolve,reject) =>{
            return authService.logIn(email,password)
                .then(result => {
                    const userData = result.data.user;
                    if(result.status === 200) {
                        if(remember) {
                            localStorage.setItem('id_token', userData.api_token);
                            localStorage.setItem('user_role', userData.type.slug);
                            localStorage.setItem('user_id', userData.id);
                        }
                        dispatch(receiveLogin(userData));
                        const { authentication } = getState();
                        RequestAPI.defaults.headers.common['Authorization'] = `Bearer ${authentication.id_token}`;
                    } else {
                        dispatch(loginError(result.message));
                    }
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(loginError(error.message));
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };

    function requestLogin() {
        return {
            type: 'LOGIN_REQUEST',
            isAuthenticated: false
        }
    }

    function receiveLogin(result) {
        return {
            type: 'LOGIN_SUCCESS',
            isAuthenticated: true,
            id_token: result.api_token,
            role: result.type.slug,
            id: result.id
        }
    }

    function loginError(message) {
        return {
            type: 'LOGIN_ERROR',
            isAuthenticated: false,
            message
        }
    }
};