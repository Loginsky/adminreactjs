import { fetchingAction } from "./fetchingAction";
import { hideLoading, showLoading } from "react-redux-loading-bar";
import registerService from "../services/registerService";

export const registerAction = (cv,nextRoute) => {
    return (dispatch,getState) => {
        dispatch(setRegisterUserCVAction(cv));
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            const { registerUser } = getState();
            return registerService.sendRegisterData(registerUser)
                .then(result => {
                    dispatch(changeRegisterRoutesAction(nextRoute));
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

export const checkEmailWhenRegister = (email, nextRoute) => {
    return dispatch => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return registerService.checkEmail(email)
                .then(result => {
                    dispatch(changeRegisterRoutesAction(nextRoute));
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

export const confirmRegisterActionThunk = (data) => {
    return dispatch => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return registerService.confirmRegister(data)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

export const proceedRegisterAction = (nextRoute) => {
    return dispatch => {
        return new Promise((resolve) =>{
            dispatch(changeRegisterRoutesAction(nextRoute));
            resolve();
        })
    };
};

function changeRegisterRoutesAction(nextRoute) {
    return {
        type: 'SET_REGISTER_ROUTES',
        nextRoute
    }
}

export const setRegistrationStatusAction = (status) => {
    return {
        type: 'SET_REGISTER_STATUS',
        status
    }
};

export const resetRegistrationRoutesAction = () => {
    return {
        type: 'RESET_REGISTER_ROUTES'
    }
};

export const setRegisterUserContactsAction = (contacts) => {
    return {
        type: 'SET_REGISTER_USER_CONTACTS',
        contacts
    }
};

export const setRegisterUserSettingsAction = (settings) => {
    return {
        type: 'SET_REGISTER_USER_SETTINGS',
        settings
    }
};

export const setRegisterUserSkillsAction = (skills) => {
    return {
        type: 'SET_REGISTER_USER_SKILLS',
        skills
    }
};




