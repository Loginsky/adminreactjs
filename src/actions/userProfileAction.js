import profileService from "../services/profileService";
import { fetchingAction } from "./fetchingAction";
import { hideLoading, showLoading } from "react-redux-loading-bar";

/** SELECT_EDITING_USER **/
export const selectEditingUserAction = (id) => {
    return {
        type: 'SELECT_EDITING_USER',
        id
    }
};

/** get start page info **/
export const getStartTabInfoActionThunk = (id) => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return profileService.getStartTabInfo(id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

/** submit adding new comment on start page **/
export const addStartCommentThunk = (data,id) => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return profileService.addStartComment(data,id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

/** get user profile contact details **/
export const getUserContactDetailsActionThunk = (id) => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return profileService.getUserContactDetails(id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    const data = {
                        title: result.data.title || '',
                        email: result.data.email || '',
                        first_name: result.data.first_name || '',
                        last_name: result.data.last_name || '',
                        phone: result.data.phone || '',
                        address: result.data.address || '',
                        postal_code: result.data.postal_code || '',
                        country_code: result.data.country_code || '',
                        is_tested: result.data.is_tested,
                        test_result: result.data.test_result || '',
                        terms_accepted: result.data.terms_accepted,
                        possible_customer: result.data.possible_customer || '',
                        priority: result.data.priority,
                        price_per_hour: result.data.price_per_hour || ''
                    };
                    resolve(data);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

/** get user types and statuses for contact details **/
export const getUserTypesStatusesActionThunk = (id) => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return profileService.getUserTypesStatuses(id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result.data);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

/** submit contact details **/
export const submitUserContactsThunk = (data,id) => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return profileService.submitUserContacts(data, id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

/** get user profile settings **/
export const getUserSettingsActionThunk = (id) => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return profileService.getUserSettings(id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    const data = {
                        availabilities: result.data.availabilities,
                        missionLengths: result.data.mission_lengths,
                        placements: result.data.placements,
                        assignmentsPreferences: result.data.assignments_preferences,
                        from_customer: result.data.from_customer || '',
                        branches:result.data.branches || '',
                        value_project: result.data.value_project || '',
                        avoid_project: result.data.avoid_project || '',
                        price_level: result.data.price_level || '',
                        wish: result.data.wish || '',
                    };
                    resolve(data);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};

/** submit user profile settings (preferences) **/
export const submitUserSettingsThunk = (data,id) => {
    return (dispatch) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return profileService.submitUserSettings(data, id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};


