import { hideLoading, showLoading } from "react-redux-loading-bar";
import { fetchingAction } from "./fetchingAction";
import userStatusesService from "../services/userStatusesService";

export const storeUserType = (userType) => {
    return {
        type: 'STORE_USER_TYPE',
        userType
    }
};

/**
 * Preferences thunk
 * @returns {function(*=)}
 */
export const getUsersTypesActionThunk = () => {
    return dispatch => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve, reject) => {
            return userStatusesService.getUsersTypes()
                .then(result => {
                    const types = result.data;
                    dispatch(storeUserType(types));
                    dispatch(hideLoading());
                    resolve(types);
                }, error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};