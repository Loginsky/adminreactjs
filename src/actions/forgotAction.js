import { showLoading, hideLoading } from 'react-redux-loading-bar'

import { fetchingAction } from './fetchingAction';

import authService from "../services/authService";

export const forgotAction = (email) => {
    return dispatch => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            return authService.forgotPassword(email)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};