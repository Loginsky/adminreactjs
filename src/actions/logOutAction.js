import { showLoading, hideLoading } from 'react-redux-loading-bar'

import { fetchingAction } from './fetchingAction';
import authService from "../services/authService";

export const logOutAction = () => {
    return dispatch => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        dispatch(requestLogOut());
        return new Promise((resolve,reject) =>{
            return authService.logOut()
                .then(result => {
                    if(result.success) {
                        localStorage.removeItem('id_token');
                        localStorage.removeItem('user_role');
                        localStorage.removeItem('user_id');
                        dispatch(receiveLogOut());
                    } else {
                        dispatch(logOutError(result.message));
                    }
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(logOutError(error));
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };

    function requestLogOut() {
        return {
            type: 'LOGOUT_REQUEST',
            isAuthenticated: true
        }
    }

    function receiveLogOut() {
        return {
            type: 'LOGOUT_SUCCESS',
            isAuthenticated: false,
            id_token: '',
            role: 'default',
            id: null
        }
    }

    function logOutError(message) {
        return {
            type: 'LOGOUT_ERROR',
            isAuthenticated: true,
            message
        }
    }
};