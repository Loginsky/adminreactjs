import authService from "../services/authService";
import {fetchingAction} from "./fetchingAction";
import {hideLoading, showLoading} from "react-redux-loading-bar";

/** submit change password **/
export const submitPasswordActionThunk = (data) => {
    return (dispatch,getState) => {
        dispatch(fetchingAction(true));
        dispatch(showLoading());
        return new Promise((resolve,reject) =>{
            const { authentication } = getState();
            return authService.changePassword(data, authentication.id)
                .then(result => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    resolve(result);
                },error => {
                    dispatch(fetchingAction(false));
                    dispatch(hideLoading());
                    reject(error);
                });
        })
    };
};