import {combineReducers} from 'redux';
import authentication from './authentication';
import translations from './translations';
import fetching from './fetching';
import notification from './notification';
import register from './register';
import skills from './skills';
import preferences from './preferences';
import registerUser from './registerUser';
import userType from './userType';
import search from './search';
import projects from './projects';
import stuff from './stuff';
import siteLocales from './siteLocales';
import editingUser from './editingUser';
import report from './report';
import {localeReducer} from 'react-localize-redux';
import {routerReducer} from 'react-router-redux';
import {loadingBarReducer} from 'react-redux-loading-bar'

const reducers = combineReducers({
    authentication,
    translations,
    fetching,
    notification,
    register,
    skills,
    preferences,
    registerUser,
    userType,
    search,
    projects,
    stuff,
    siteLocales,
    editingUser,
    report,
    locale: localeReducer,
    loadingBar: loadingBarReducer,
    routerReducer
});

export default reducers;