const init = {
    show: false,
    error: false,
    message:''
};

function notification(state = init,action) {
    switch (action.type) {
        case "SET_NOTIFICATION":
            return {...state, show: action.show, error: action.error, message: action.message};
        default:
            return state;
    }
}

export default notification;