import config from '../config/config'

const init = {
    routes: config.routes.register,
    status: false
};

function register(state = init,action) {
    switch (action.type) {
        case "SET_REGISTER_ROUTES":
            return {...state, routes: state.routes.map((route)=>{
                    if(action.nextRoute === route.route) { route.allowed = true; }
                    return route;
                })};
        case "RESET_REGISTER_ROUTES":
            return {...state, routes: state.routes.map((route,index)=>{
                    if(index) {
                        route.allowed = false;
                    }
                    return route;
                })};
        case "SET_REGISTER_STATUS":
            return {...state, status: action.status };
        default:
            return state;
    }
}

export default register;