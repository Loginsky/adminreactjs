const init = {
    isAuthenticated: !!localStorage.getItem('id_token'),
    id_token: localStorage.getItem('id_token') || '',
    role: localStorage.getItem('user_role') || 'default',
    id: localStorage.getItem('user_id') || null
};

function authentication(state = init,action) {
    switch (action.type) {
        case "LOGIN_REQUEST":
            return {...state, isAuthenticated: action.isAuthenticated};
        case "LOGIN_SUCCESS":
            return {...state, isAuthenticated: action.isAuthenticated, id_token:action.id_token, role:action.role, id:action.id};
        case "LOGIN_ERROR":
            return {...state, isAuthenticated: action.isAuthenticated};
        case "LOGOUT_REQUEST":
            return {...state, isAuthenticated: action.isAuthenticated};
        case "LOGOUT_SUCCESS":
            return {...state, isAuthenticated: action.isAuthenticated, id_token:action.id_token, role:action.role, id:action.id};
        case "LOGOUT_ERROR":
            return {...state, isAuthenticated: action.isAuthenticated};
        default:
            return state;
    }
}

export default authentication;