const init = {};

function preferences(state = init,action) {
    switch (action.type) {
        case "SET_AVAILABILITIES_PREFERENCES":
            return {...state, availabilities: action.availabilities};
        case "SET_MISSION_LENGTHS_PREFERENCES":
            return {...state, missionLengths: action.missionLengths};
        case "SET_PLACEMENTS_PREFERENCES":
            return {...state, placements: action.placements};
        case "SET_ASSIGNMENTS_PREFERENCES":
            return {...state, assignmentsPreferences: action.assignmentsPreferences};
        case "SET_CAPABILITIES_PREFERENCES":
            return {...state, capabilities: action.capabilities};
        case "SET_YEARS_INDUSTRY_PREFERENCES":
            return {...state, years: action.years};
        default:
            return state;
    }
}

export default preferences;