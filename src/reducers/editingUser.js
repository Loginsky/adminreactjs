const init = {
    selectedUser: null
};

function editingUser(state = init,action) {
    switch (action.type) {
        case "SELECT_EDITING_USER":
            return {...state, selectedUser: action.id};
        default:
            return state;
    }
}

export default editingUser;