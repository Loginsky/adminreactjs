const init = {};

function translations (state = init, action) {
    switch (action.type) {
        case "SET_TRANSLATIONS":
            return {...state, ...action.payload};
        default:
            return state;
    }
}

export default translations;