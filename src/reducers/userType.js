const init = [];

function userType(state = init, action) {
    switch (action.type) {
        case "STORE_USER_TYPE":
            return action.userType;
        default:
            return state;
    }
}

export default userType;