const init = {
    isFetching: false
};

function fetching(state = init,action) {
    switch (action.type) {
        case "SET_FETCHING":
            return {...state, isFetching: action.payload};
        default:
            return state;
    }
}

export default fetching;