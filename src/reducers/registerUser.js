const init = {
    contacts: {
        email:'',
        first_name:'',
        last_name:'',
        phone:'',
        address:'',
        postal_code:'',
        country_code:''
    },
    settings: {
        availabilities: [],
        missionLengths: [],
        placements: [],
        assignmentsPreferences: [],
        from_customer:'',
        branches:'',
        value_project: '',
        avoid_project: '',
        price_level: '',
        wish: '',
    },
    skills: {
        selected: [],
        additional: ''
    },
    cv:{
        image_file:{},
        title_2: '',
        additional_description: '',
        capabilities: [],
        years: 1,
        experiences: [],
        educations:[],
        certifications:[],
        languages:[],
        linkedin_url:'',
        github_url:'',
        website_url:''
    },
    role: 'default'
};

function registerUser(state = init,action) {
    switch (action.type) {
        case "SET_REGISTER_USER_CONTACTS":
            return {...state, contacts: action.contacts};
        case "SET_REGISTER_USER_SETTINGS":
            return {...state, settings: action.settings};
        default:
            return state;
    }
}

export default registerUser;

