import RequestAPI from "../helpers/server";

export default class authService {
    static logIn(email,password) {
        return new Promise((resolve, reject) => {
            const url = `/api/login`;
            const data = {
                email:email,
                password:password
            };
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }

    static forgotPassword(email) {
        return new Promise((resolve, reject) => {
            // simulate server response
            setTimeout(()=>{
                resolve({success: true, message: 'Your new password has been sent to: ' + email});
            },1000)
        });
    }

    static logOut() {
        return new Promise((resolve, reject) => {
            // simulate server response
            setTimeout(()=>{
                resolve({success: true});
            },1000)
        });
    }

    static changePassword(data,id) {
        console.log(data,id);
        return new Promise((resolve, reject) => {
            const url = `/api/password/change/${id}`;
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }
}