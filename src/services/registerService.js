import RequestAPI from "../helpers/server";

export default class registerService {
    static sendRegisterData(data) {
        return new Promise((resolve, reject) => {
            const url = '/api/register';
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }

    static checkEmail(data) {
        return new Promise((resolve, reject) => {
            const url = 'api/register/check_email';
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }

    static confirmRegister(data) {
        return new Promise((resolve, reject) => {
            const url = 'api/register/confirm';
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }
}

