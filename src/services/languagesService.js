import RequestAPI from "../helpers/server";

export default class languagesService {
    static getLanguagesList() {
        return new Promise((resolve,reject) => {
            const url = `/api/languages`;
            RequestAPI.get(url)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }

    static getLocales() {
        return new Promise((resolve,reject) => {
            // ToDo: remove after back-end finished
            setTimeout(()=>{
                resolve([{id: 1, name: "English", code: "en"}, {id: 2, name: "Sweden", code: "se"}])
            },1000)
            // ToDo: uncomment after back-end finished
            // const url = `/api/locales`;
            // RequestAPI.get(url)
            //     .then(res => resolve(res))
            //     .catch(error => reject(error));
        });
    }
}

