import RequestAPI from "../helpers/server";

export default class userStatusesService {
    static getUsersTypes() {
        return new Promise((resolve,reject) => {
            const url = '/api/users/statuses';
            RequestAPI.get(url)
                .then(res => resolve(res))
                .catch(error => reject(error));
        });
    }
}