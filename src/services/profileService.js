import RequestAPI from "../helpers/server";

export default class profileService {

    /** START TAB **/
    // get start tab info
    static getStartTabInfo(id) {
        return new Promise((resolve, reject) => {
            const url = `/api/profile/${id}/start`;
            RequestAPI.get(url)
                .then(res => resolve(res))
                .catch(error => reject(error));
        })
    }

    // add comment from start tab
    static addStartComment(data, id) {
        return new Promise((resolve, reject) => {
            const url = `/api/profile/${id}/start`;
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        })
    }

    /** CONTACT INFORMATION TAB **/
    // get company types for legal information
    static getUserContactDetails(id) {
        return new Promise((resolve, reject) => {
            const url = `/api/profile/${id}/contacts`;
            RequestAPI.get(url)
                .then(res => resolve(res))
                .catch(error => reject(error));
        })
    }

    static getUserTypesStatuses(id) {
        return new Promise((resolve, reject) => {
            // ToDo: uncomment when back-end will be finished and update
            // const url = `/api/users/${id}/statuses`;
            const url = `/api/users/statuses`;
            RequestAPI.get(url)
                .then(res => resolve(res))
                .catch(error => reject(error));
        })
    }

    static submitUserContacts(data, id) {
        return new Promise((resolve, reject) => {
            const url = `/api/profile/${id}/contacts`;
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        })
    }

    /** PREFERENCES SETTINGS TAB **/
    // get company types for legal information
    static getUserSettings(id) {
        return new Promise((resolve, reject) => {
            const url = `/api/profile/${id}/settings`;
            RequestAPI.get(url)
                .then(res => resolve(res))
                .catch(error => reject(error));
        })
    }

    static submitUserSettings(data, id) {
        return new Promise((resolve, reject) => {
            const url = `/api/profile/${id}/settings`;
            RequestAPI.post(url, data)
                .then(res => resolve(res))
                .catch(error => reject(error));
        })
    }

    static getContacts() {
        const url = `/api/settings/stuff`;
        return RequestAPI.get(url).then(res => res.data);
    }
}