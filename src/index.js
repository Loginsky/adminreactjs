import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { initialize, addTranslation } from 'react-localize-redux';
import { ConnectedRouter } from 'react-router-redux'

import history from './helpers/history'
import store from './helpers/store'

import App from './App'

// languages
const languages = ['en', 'ua'];
const defLang = 'en';

// Todo: remove if unnecessary
const missingTranslationMsg = 'Missed translation for key: "$/{key/}" and language: "$/{code/}"!';

store.dispatch(initialize(languages, { defaultLanguage: defLang, missingTranslationMsg }));
store.dispatch(addTranslation(store.getState().translations));
//
render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App history={history}/>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'),
);