import React, { Component } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";

// css
import './Notification.css';

class Notification extends Component {
    render() {
        const { message, error} = this.props;
        return(
            <div className={classNames('notification', {'error': error})}>{message}</div>
        )
    }
}

Notification.propTypes = {
    message: PropTypes.string,
    error: PropTypes.bool,
};

export default Notification;