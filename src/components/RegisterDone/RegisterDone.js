import React, {Component} from "react";
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { getTranslate } from "react-localize-redux";

// actions
import { resetRegistrationRoutesAction } from "../../actions/registerAction";
import { setRegistrationStatusAction } from "../../actions/registerAction";
import { setRegisterUserContactsAction,
         setRegisterUserSettingsAction,
         setRegisterUserSkillsAction,
         setRegisterUserCVAction } from "../../actions/registerAction";

// css
import './RegisterDone.css';

class RegisterDone extends Component {
    componentDidMount() {
        this.props.setRegisterStatus(false);
        this.props.resetRegistrationRoutes();

        this.props.resetRegisterContacts({
            email:'',
            first_name:'',
            last_name:'',
            phone:'',
            address:'',
            postal_code:'',
            country_code:''
        })
        this.props.resetRegisterSettings({
            availabilities: [],
            missionLengths: [],
            placements: [],
            assignmentsPreferences: [],
            from_customer:'',
            branches:'',
            value_project: '',
            avoid_project: '',
            price_level: '',
            wish: '',
        })
        this.props.resetRegisterSkills({
            selected: [],
            additional: ''
        })
        this.props.resetRegisterCV({
            image_file:{},
            title_2: '',
            additional_description: '',
            capabilities: [],
            years: 1,
            experiences: [],
            educations:[],
            certifications:[],
            languages:[],
            linkedin_url:'',
            github_url:'',
            website_url:''
        })
    }

    render() {
        return(
            <div>
                <h2>{this.props.translate('REGISTER_CONGRATULATIONS_TITLE', null, {missingTranslationMsg: 'Congratulations!'})}</h2>
                <p>{this.props.translate('REGISTER_CONGRATULATIONS_TEXT', null, {missingTranslationMsg: 'We\'ve sent you password to you email!'})}</p>
                <Link to='/'>{this.props.translate('REGISTER_CONGRATULATIONS_LINK_LABEL', null, {missingTranslationMsg: 'You can now log in here'})}</Link>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetRegistrationRoutes: () => {
            return dispatch(resetRegistrationRoutesAction());
        },
        setRegisterStatus: (status) => {
            return dispatch(setRegistrationStatusAction(status));
        },
        resetRegisterSettings: (data) => {
            return dispatch(setRegisterUserContactsAction(data));
        },
        resetRegisterContacts: (data) => {
            return dispatch(setRegisterUserSettingsAction(data));
        },
        resetRegisterSkills: (data) => {
            return dispatch(setRegisterUserSkillsAction(data));
        },
        resetRegisterCV: (data) => {
            return dispatch(setRegisterUserCVAction(data));
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps) (RegisterDone);