import React from 'react'
import { Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from "prop-types";

// config
import config from '../config/config';

const RegisterRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) => {
            const redirectPath = rest.isAuthenticated ? config.roles[rest.role].rootRoute : '/';
            return (
                (rest.isAuthenticated || !rest.registerStatus) ? (
                    <Redirect
                        to={{
                            pathname: redirectPath,
                            state: { from: props.location }
                        }}
                    />
                ) : (
                    <Component {...props} />
                )
            )
        }}
    />
);

RegisterRoute.propTypes = {
    isAuthenticated:PropTypes.bool,
    role:PropTypes.string,
    registerStatus:PropTypes.bool
};

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.authentication.isAuthenticated,
        role: state.authentication.role,
        registerStatus: state.register.status
    }
};

export default withRouter(connect(mapStateToProps,null)(RegisterRoute));