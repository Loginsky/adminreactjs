import React, {Component} from "react";
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

// css
import './Login.css';

class LogIn extends Component {
    render() {
        const Fragment = React.Fragment;
        return(
            <Fragment>
                <div className="content">
                    <form onSubmit={(e)=>{this.props.submitLogin(e,['email','password'])}}>
                        <div className="form-field text-field">
                            <label className="for-inputs" htmlFor="email">{this.props.translate('EMAIL_LABEL', null, {missingTranslationMsg: 'Email'})}</label>
                            <input id='email'
                                   name='email'
                                   onChange={(e) => this.props.handlerChange(e,'email')}
                                   type="email"
                                   value={this.props.email}/>
                            <div className="valid-error">
                                {!this.props.isValid.email ? <span>{this.props.translate('EMAIL_IS_REQUIRED_VALIDATION', null, {missingTranslationMsg: 'Email is required'})}</span> : null}
                            </div>
                        </div>
                        <div className="form-field text-field">
                            <label className="for-inputs" htmlFor="password">{this.props.translate('PASSWORD_LABEL', null, {missingTranslationMsg: 'Password'})}</label>
                            <input id='password'
                                   name='password'
                                   onChange={(e) => this.props.handlerChange(e,'password')}
                                   type="password"
                                   value={this.props.password}/>
                            <div className="valid-error">
                                {!this.props.isValid.password ? <span>{this.props.translate('PASSWORD_IS_REQUIRED_VALIDATION', null, {missingTranslationMsg: 'Password is required'})}</span> : null}
                            </div>
                        </div>
                        <div className="form-field checkbox-field">
                            <input type="checkbox"
                                   checked={this.props.remember}
                                   onChange={(e) => this.props.handlerCheckChange(e)}
                                   id="remember_me"/>
                            <label className="for-inputs" htmlFor="remember_me">{this.props.translate('KEEP_ME_LOGGED_IN_LABEL', null, {missingTranslationMsg: 'Keep me logged in'})}</label>

                        </div>
                        <div className="form-field submit-field">
                            <input className="btn-main" type="submit" value={this.props.translate('LOG_IN_BUTTON', null, {missingTranslationMsg: 'Log In'})}/>
                        </div>
                    </form>
                    <a className="forgot-password" onClick={(e)=>this.props.toggleForgot(e)}>{this.props.translate('FORGOT_PASSWORD_LABEL', null, {missingTranslationMsg: 'Forgot password?'})}</a>
                    {this.props.showForgot ?
                        <form className="forgot-pass-form" onSubmit={(e)=>{this.props.submitForgot(e,['emailForgot'])}}>
                            <div className="form-field text-field">
                                <label className="for-inputs" htmlFor="emailForgot">{this.props.translate('EMAIL_LABEL', null, {missingTranslationMsg: 'Email'})}</label>
                                <input id='emailForgot'
                                       name='emailForgot'
                                       onChange={(e) => this.props.handlerChange(e,'emailForgot')}
                                       type="email"
                                       value={this.props.emailForgot}/>
                                <div className="valid-error">
                                    {!this.props.isValid.emailForgot ? <span>{this.props.translate('EMAIL_IS_REQUIRED_VALIDATION', null, {missingTranslationMsg: 'Email is required'})}</span> : null}
                                </div>
                            </div>
                            <div className="form-field submit-field">
                                <input className="btn-main" type="submit" value={this.props.translate('SEND_BUTTON', null, {missingTranslationMsg: 'Send'})}/>
                            </div>
                        </form>
                        : null }
                </div>
                <div className="frame-btn">
                    <Link className="btn-register" to='/register' onClick={this.props.clickRegister}>{this.props.translate('SIGN_UP_BUTTON', null, {missingTranslationMsg: 'Sign Up'})}</Link>
                </div>
            </Fragment>
        )
    }
}

LogIn.propTypes = {
    email:PropTypes.string,
    emailForgot:PropTypes.string,
    password:PropTypes.string,
    remember:PropTypes.bool,
    isValid: PropTypes.object,
    toggleForgot:PropTypes.func,
    submitLogin:PropTypes.func,
    submitForgot:PropTypes.func,
    handlerChange:PropTypes.func,
    handlerCheckChange:PropTypes.func,
    translate:PropTypes.func,
    clickRegister:PropTypes.func
};

export default LogIn;