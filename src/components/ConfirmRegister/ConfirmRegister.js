import React, { Component } from "react";
import { Link } from 'react-router-dom';
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getTranslate } from "react-localize-redux";

// thunk
import { confirmRegisterActionThunk } from '../../actions/registerAction';

class ConfirmRegister extends Component {

    constructor(...props) {
        super(...props);

        const params = new URLSearchParams(this.props.location.search);
        const activate_token = params.get('token');

        this.state = {
            activate_token: activate_token,
            waiting: true,
            confirmation: false,
            error_message:''
        }
    }

    componentWillMount() {
        const data = {activate_token:this.state.activate_token};
        this.props.confirmRegister(data)
            .then(result=>{
                console.info(result);
                this.setState({confirmation:true,waiting:false});
            },error=>{
                console.warn(error);
                this.setState({confirmation:false,waiting:false,error_message:error.message});
            });
    }

    render() {
        return(
            <div className="confirm-frame">
                <h2 className="reg-title">{this.props.translate('CONFIRMATION_HEADER', null, {missingTranslationMsg: 'Register Confirmation'})}</h2>
                {this.state.waiting ?
                    <p>{this.props.translate('WAITING_CONFIRMATION', null, {missingTranslationMsg: 'Waiting for your confirmation...'})}</p>
                    :
                    (this.state.confirmation ?
                        <p className="success">
                            {this.props.translate('CONFIRMATION_APPROVED', null, {missingTranslationMsg: 'Your confirmation has been approved, please check you mail and'})}
                            &nbsp;
                            <Link to={'/'}>{this.props.translate('LOGIN', null, {missingTranslationMsg: 'login'})}</Link>
                        </p> : <p className="error">{this.state.error_message}</p>
                    )
                }
            </div>
        )
    }
}

ConfirmRegister.propTypes = {
    translate:PropTypes.func,
    confirmRegister:PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        confirmRegister: (data)=>{
            return dispatch(confirmRegisterActionThunk(data))
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps) (ConfirmRegister);