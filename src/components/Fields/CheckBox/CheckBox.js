import React from "react";
import {getTranslate} from "react-localize-redux";
import {connect} from "react-redux";

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {faCheck} from "@fortawesome/fontawesome-free-solid";

const CheckBox = (props) => {
    const {translate, id, name, checked, translate_key, title, handler} = props;
    return (
        <div className="form-field">
            <label className="checkbox-label">
                <input id={name}
                       name={name}
                       type="checkbox"
                       checked={checked}
                       onChange={(e) => handler(e, id)}/>
                {title}
                {/*{translate(translate_key, null, {missingTranslationMsg: title})}*/}
                <div className="icon-checked">
                    <FontAwesomeIcon icon={faCheck} color={'#98c449'}/>
                </div>
            </label>
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale)
    }
};

export default connect(mapStateToProps)(CheckBox);