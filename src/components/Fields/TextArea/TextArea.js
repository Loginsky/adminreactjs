import React from "react";
import { getTranslate } from "react-localize-redux";
import { connect } from "react-redux";

const TextArea = (props) => {
    const { translate, id, name, value, translate_key, title, validation_translate_key, validation_title, required, handler, isValid, placeholder, placeholder_translation_key } = props;
    return(
        <div className="form-field">
            <label className="for-inputs" htmlFor={name}>{translate(translate_key, null, {missingTranslationMsg: title})}{required ? <span>*</span> : null}</label>
            <textarea
                id={id}
                name={name}
                onChange={(e) => handler(e,id)}
                value={value}
                placeholder={placeholder ? translate(placeholder_translation_key, null, {missingTranslationMsg: placeholder}) : ''}
            ></textarea>
            {required ?
                <div className="valid-error">
                    {!isValid ? <span>{translate(validation_translate_key, null, {missingTranslationMsg: validation_title})}</span> : null}
                </div>
                : null}
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale)
    }
};

export default connect(mapStateToProps) (TextArea);
