import React from "react";
const InputPassword = (props) => {
    const {
        id,
        name,
        value,
        title,
        validation_title,
        required,
        handler,
        isValid
    } = props;
    return (
        <div className="form-field text-field">
            <label className="for-inputs"
                   htmlFor={name}>{title}{required ?
                <span>*</span> : null}</label>
            <input id={id}
                   name={name}
                   onChange={(e) => handler(e, id)}
                   type="password"
                   value={value}/>
            {required ?
                <div className="valid-error">
                    {!isValid ?
                        <span>{validation_title}</span> : null}
                </div>
                : null}
        </div>
    )
};

export default InputPassword;