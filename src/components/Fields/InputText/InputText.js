import React from "react";
import {getTranslate} from "react-localize-redux";
import {connect} from "react-redux";

const InputText = (props) => {
    const {
        translate,
        id,
        name,
        value,
        translate_key,
        title,
        validation_translate_key,
        validation_title,
        required,
        customValidation,
        handler,
        isValid
    } = props;
    return (
        <div className="form-field text-field">
            <label className="for-inputs"
                   htmlFor={name}>{translate(translate_key, null, {missingTranslationMsg: title})}{required ?
                <span>*</span> : null}</label>
            <input id={id}
                   name={name}
                   onChange={(e) => handler(e, id)}
                   type="text"
                   value={value}/>
            {required || customValidation ?
                <div className="valid-error">
                    {!isValid ?
                        <span>{translate(validation_translate_key, null, {missingTranslationMsg: validation_title})}</span> : null}
                </div>
                : null}
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale)
    }
};

export default connect(mapStateToProps)(InputText);