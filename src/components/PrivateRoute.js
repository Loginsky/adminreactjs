import React from 'react'
import {Route, Redirect, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';

// config
import config from '../config/config'

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={(props) => {
            if (rest.isAuthenticated) {
                if ([...config.roles[rest.role].menu, ...config.roles[rest.role].availableRoutes].some(item => item.route === rest.path)) {
                    return <Component {...props} />
                } else {
                    return <Redirect to={"/"}/>
                }
            } else {
                return (<Redirect to={{pathname: "/", state: {from: props.location}}}/>)
            }
        }
        }
    />
);

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.authentication.isAuthenticated,
        role: state.authentication.role
    }
};

export default withRouter(connect(mapStateToProps, null)(PrivateRoute));