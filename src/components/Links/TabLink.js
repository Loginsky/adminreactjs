import React from 'react';
import { Route, Link } from 'react-router-dom';

const TabLink = ({ label, to, activeOnlyWhenExact }) => (
    <Route
        path={to}
        exact={activeOnlyWhenExact}
        children={({ match }) => {
            return (
                <li className={match ? "active" : ""}>
                    {<Link to={to}>{label}</Link>}
                </li>
            )
        } }
    />
);

export default TabLink;