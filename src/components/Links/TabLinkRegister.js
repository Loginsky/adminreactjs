import React from 'react';
import { Route, Link } from 'react-router-dom';

const TabLinkRegister = ({ label, to, allowed, activeOnlyWhenExact }) => (
    <Route
        path={to}
        exact={activeOnlyWhenExact}
        children={({ match }) => {
            return (
                <li className={match ? "active" : ""}>
                    { allowed ? <Link to={to}>{label}</Link> : <a onClick={(e)=>{e.preventDefault()}}>{label}</a> }
                </li>
            )
        } }
    />
);

export default TabLinkRegister;