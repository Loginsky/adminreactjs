import React, {Component} from "react";
import PropTypes from "prop-types";

// css
import './ContentMenu.css';

class ContentMenu extends Component {
    render() {
        const { menuTabs } = this.props;
        return (
            <div className="content-menu">
                { menuTabs }
            </div>
        )
    }
}

ContentMenu.propTypes = {
    menuTabs:PropTypes.node
};

export default ContentMenu;