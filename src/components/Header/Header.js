import React, { Component } from "react";
import PropTypes from "prop-types";

// css
import './Header.css';

class Header extends Component {
    render() {
        const { menuItems, logo, subMenuItems, subMenuOpen } = this.props;
        return(
            <header className='header'>
                <div className='container'>
                    {logo}
                    <nav className='menu'>{menuItems}</nav>
                    {subMenuOpen &&
                        <nav className='sub-menu'>
                            {subMenuItems}
                        </nav>
                    }
                </div>
            </header>
        )
    }
}

Header.propTypes = {
    menuItems: PropTypes.node,
    logo: PropTypes.node
};

export default Header;