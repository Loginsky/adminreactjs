import React, {Component} from "react";
import PropTypes from 'prop-types';
import Select from 'react-select';
import classNames from 'classnames';

// css
import './ContactDetails.css';

// config
import config from '../../config/config'

// components
import InputEmail from '../Fields/InputEmail/InputEmail';
import InputText from '../Fields/InputText/InputText';
import CheckBox from '../Fields/CheckBox/CheckBox';
import TextArea from '../Fields/TextArea/TextArea';
import StarRate from '../../containers/StarRate'

class ContactDetails extends Component {
    getFields() {
        const {
            role,
            contacts,
            isValid,
            handlerChange,
            handlerCheckboxChange,
            translate,
            user_types,
            user_statuses,
            changeUserType,
            changeUserStatus
        } = this.props;
        return config.roles[role].contact_details_fields.map((field_name,index)=> {
            switch (field_name) {
                case 'title':
                    return <InputText key={index}
                                      id='title'
                                      name='title'
                                      value={contacts.title}
                                      translate_key='TITLE_LABEL'
                                      title='Title'
                                      handler={handlerChange}/>;
                case 'email':
                    return <InputEmail key={index}
                                      id='email'
                                      name='email'
                                      value={contacts.email}
                                      translate_key='EMAIL_LABEL'
                                      title='Email'
                                      handler={handlerChange}
                                      validation_translate_key='INVALID_EMAIL_VALIDATION'
                                      validation_title='Invalid email'
                                      required={true}
                                      isValid={isValid.email}/>;
                case 'address':
                    return <InputText key={index}
                                       id='address'
                                       name='address'
                                       value={contacts.address}
                                       translate_key='ADDRESS_LABEL'
                                       title='Address'
                                       handler={handlerChange}/>;
                case 'first_name':
                    return <InputText key={index}
                                      id='first_name'
                                      name='first_name'
                                      value={contacts.first_name}
                                      translate_key='FIRST_NAME_LABEL'
                                      title='First name'
                                      handler={handlerChange}
                                      validation_translate_key='INVALID_FIRST_NAME_VALIDATION'
                                      validation_title='Invalid first name'
                                      required={true}
                                      isValid={isValid.first_name}/>;
                case 'postal_code':
                    return <InputText key={index}
                                      id='postal_code'
                                      name='postal_code'
                                      value={contacts.postal_code}
                                      translate_key='POST_INDEX_LABEL'
                                      title='Post index'
                                      handler={handlerChange}/>;
                case 'last_name':
                    return <InputText key={index}
                                      id='last_name'
                                      name='last_name'
                                      value={contacts.last_name}
                                      translate_key='LAST_NAME_LABEL'
                                      title='Last name'
                                      handler={handlerChange}
                                      validation_translate_key='INVALID_LAST_NAME_VALIDATION'
                                      validation_title='Invalid last name'
                                      required={true}
                                      isValid={isValid.last_name}/>;
                case 'country_code':
                    return <InputText key={index}
                                      id='country_code'
                                      name='country_code'
                                      value={contacts.country_code}
                                      translate_key='ORT_LABEL'
                                      title='Ort'
                                      handler={handlerChange}
                                      validation_translate_key='INVALID_ORT_VALIDATION'
                                      validation_title='Invalid ort'
                                      required={true}
                                      isValid={isValid.country_code}/>;
                case 'phone':
                    return <InputText key={index}
                                      id='phone'
                                      name='phone'
                                      value={contacts.phone}
                                      translate_key='PHONE_LABEL'
                                      title='Phone'
                                      handler={handlerChange}
                                      validation_translate_key='INVALID_PHONE_VALIDATION'
                                      validation_title='Invalid phone'
                                      required={true}
                                      isValid={isValid.phone}/>;
                case 'is_tested':
                    return <CheckBox key={index}
                                      id='is_tested'
                                      name='is_tested'
                                      checked={contacts.is_tested}
                                      title={translate('TEST', null, {missingTranslationMsg: 'Test'})}
                                      handler={handlerCheckboxChange}/>;
                case 'test_result':
                    return <InputText key={index}
                                      id='test_result'
                                      name='test_result'
                                      value={contacts.test_result}
                                      translate_key='TEST_RESULT'
                                      title='Test result'
                                      handler={handlerChange}/>;
                case 'terms_accepted':
                    return <CheckBox key={index}
                                     id='terms_accepted'
                                     name='terms_accepted'
                                     checked={contacts.terms_accepted}
                                     title={translate('TERMS_ACCEPTED', null, {missingTranslationMsg: 'I accept the terms'})}
                                     handler={handlerCheckboxChange}/>;
                case 'possible_customer':
                    return <TextArea key={index}
                                     id='possible_customer'
                                     name='possible_customer'
                                     value={contacts.possible_customer}
                                     translate_key='POSSIBLE_CUSTOMER'
                                     title='Possible customer'
                                     handler={handlerChange}/>;
                case 'priority':
                    return <div key={index} className="form-field">
                                <label className="for-inputs">
                                    {translate('USER_PRIORITY', null, {missingTranslationMsg: 'User priority'})}
                                </label>
                                <StarRate key={index}
                                          value={contacts.priority}
                                          count={3}
                                          changeRate={(val)=>this.props.handleChangePriority(val,'priority')}/>
                          </div>;
                case 'price_per_hour':
                    return <TextArea key={index}
                                     id='price_per_hour'
                                     name='price_per_hour'
                                     value={contacts.price_per_hour}
                                     translate_key='PRICE_PER_HOUR'
                                     title='Price per hour'
                                     handler={handlerChange}/>;
                case 'type_id':
                    return  <div key={index} className="form-field">
                                <label className="for-inputs">
                                    {translate('USER_TYPE', null, {missingTranslationMsg: 'User type'})}
                                </label>
                                <Select name="type_id"
                                        value={contacts.type_id}
                                        searchable={false}
                                        clearable={false}
                                        options={user_types}
                                        onChange={(e) => changeUserType(e)}/>
                            </div>;
                case 'status_id':
                    return <div key={index} className="form-field">
                                <label className="for-inputs">
                                    {translate('USER_STATUS', null, {missingTranslationMsg: 'User status'})}
                                </label>
                                <Select name="status_id"
                                        value={contacts.status_id}
                                        searchable={false}
                                        clearable={false}
                                        options={user_statuses}
                                        onChange={(e) => changeUserStatus(e)}/>
                          </div>;
                default:
                    return null;
            }
        })
    }

    render() {
        const submitButtonLabel = this.props.isRegisterMode ? this.props.translate('PROCEED_BUTTON', null, {missingTranslationMsg: 'Proceed'}) : this.props.translate('SAVE_BUTTON', null, {missingTranslationMsg: 'Save'});
        return(
            <div className={classNames('contact-details-wrapper', this.props.role)}>
                <h2 className="reg-title">{this.props.translate('REGISTER_CONTACT_DETAILS_HEADER', null, {missingTranslationMsg: 'Contact Details'})}</h2>
                <form onSubmit={(e)=>{this.props.submitForm(e)}}>
                    {this.getFields()}
                    <div className="form-field submit-field">
                        <input className="btn-main btn-next" type="submit" value={submitButtonLabel}/>
                    </div>
                </form>
            </div>
        )
    }

}

ContactDetails.propTypes = {
    contacts: PropTypes.object,
    isValid: PropTypes.object,
    handlerChange:PropTypes.func,
    handlerCheckboxChange:PropTypes.func,
    handleChangePriority:PropTypes.func,
    translate:PropTypes.func,
    submitForm: PropTypes.func,
    isRegisterMode:PropTypes.bool,
    role: PropTypes.string,
    user_types: PropTypes.array,
    user_statuses: PropTypes.array,
    changeUserType: PropTypes.func,
    changeUserStatus: PropTypes.func
};

export default ContactDetails;