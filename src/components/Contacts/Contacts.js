import React, {Component} from "react";

// css
import './Contacts.css';

import profileService from './../../services/profileService';

class Contacts extends Component {

    constructor(props) {
        super(props);

        this.state = {
            generalInfo: {
                legal_address: '',
                slack: "http://test"
            },
            contacts: []
        };

        profileService.getContacts().then(data => {
            this.setState(() => {
                return {
                    contacts: data
                }
            })
        });
    }

    render() {
        const {generalInfo, contacts} = this.state;
        return (
            <div className="c\[ontent contacts-page">
                <div className="reg-title">Contacts</div>
                <div className="contacts-address">{generalInfo.legal_address}</div>
                <a href={generalInfo.slack} className="contacts-link">Slack Channel</a>


                <div className="">
                    <div className="reg-title">Madlemmar</div>
                    <ul className="contacts-list">
                        {contacts.map((item, index) => {
                            return <li key={index}>
                                <div className="avatar">
                                    <img src={item.avatar} alt=""/>
                                </div>
                                <div className="name">{item.name}</div>
                                <div className="sub">{item.description}</div>
                                <div className="sub">{item.email}</div>
                                <div className="sub">{item.phone}</div>
                            </li>
                        })}
                    </ul>
                </div>

            </div>
        )
    }
}

export default Contacts;