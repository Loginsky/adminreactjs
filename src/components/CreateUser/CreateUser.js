import React, {Component} from "react";
import {connect} from "react-redux";

import classNames from 'classnames';
import InputText from './../Fields/InputText/InputText';

import searchService from "./../../services/searchService";

// css
import './CreateUser.css';
import {storeUserSearch} from "../../actions/searchAction";

class CreateUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            email: '',
            valid: true
        };

    }

    onInputChange = (e) => {
        const {value} = e.target;
        this.setState(() => ({email: value}));
    };

    validateEmail = email => {
        // eslint-disable-next-line
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    addUser = () => {
        if (this.validateEmail(this.state.email)) {
            this.setState(() => ({
                valid: true,
                open: false
            }));
            searchService.createCandidate(this.state.email).then(data => {
                console.log(data);
            })
        } else {
            this.setState(() => ({valid: false}));
        }
    };

    render() {
        return (
            <div className={classNames('search-filter-frame', {opened: this.state.open})}>

                <button className="btn-main" onClick={() => {
                    this.setState(() => ({open: !this.state.open}))
                }}>New user
                </button>
                <div className="frame-create-user">
                    <InputText
                        id='create_user'
                        name='create_user'
                        value={this.state.email}
                        translate_key='LINKEDIN_URL'
                        title='Create user'
                        handler={this.onInputChange}
                    />
                    {!this.state.valid && <div>Not Valid</div>}
                    <button className="btn-main" onClick={this.addUser}>Create</button>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        userType: state.userType
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        storeUserSearch: (data) => {
            return dispatch(storeUserSearch(data))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser);
