import React, {Component} from "react";
import {connect} from "react-redux";
import { getTranslate } from "react-localize-redux";
import PropTypes from "prop-types";

// components
import InputPassword from '../Fields/InputPassword/InputPassword';

// css
import './Password.css';

// actions
import { notificationAction } from "../../actions/notificationAction";

// thunk
import { submitPasswordActionThunk } from '../../actions/userPasswordAction';

class Password extends Component {

    constructor(...props) {
        super(...props);

        this.state = {
            current_password: '',
            new_password:'',
            repeat_password:'',
            isValid: {
                current_password: true,
                new_password: true,
                repeat_password: true,
            },
            invalidTitles: {
                current_password: '',
                new_password: '',
                repeat_password: '',
            }
        };

        this.submitForm = this.submitForm.bind(this);
        this.handlerChange = this.handlerChange.bind(this)
    }

    /** mian submit **/
    submitForm(e) {
        e.preventDefault();
        if(this.validateRequiredFields() && this.validateRepeat()) {
            const data = {
                current_password: this.state.current_password,
                new_password: this.state.new_password,
                repeat_password: this.state.repeat_password,
            };
            this.props.submitPassword(data).then(result=>{
                console.info(result);
                // ToDo: ask about different messages
                if(result.status === 200) {
                    this.showNotify(true, false, this.props.translate('PASSWORD_CHANGED', null, {missingTranslationMsg: 'The password has been changed'}));
                } else {
                    this.showNotify(true, true, this.props.translate('INVALID_PASSWORD', null, {missingTranslationMsg: 'Could not save, check your current password'}));
                }
            },error=>{
                console.error(error.message);
                this.showNotify(true, true, this.props.translate('BAD_SERVER_REQUEST', null, {missingTranslationMsg: 'Sorry... bad server request...'}));
            });
        }
    }

    /** notifications **/
    showNotify(show, error, message) {
        this.props.notify(show, error, message);
        setTimeout(() => {
            this.props.notify();
        }, 5000);
    }

    /** validation for required fields **/
    validateRequiredFields() {
        const { translate } = this.props;
        let isValidate = true;
        let validate = {};
        let invalidTitles = {};
        for (let key in this.state.isValid) {
            validate = {...validate, [key]: this.state[key].toString().trim().length > 0};
            if (!this.state[key].toString().trim().length > 0) {
                invalidTitles = {...invalidTitles, [key]: translate('INVALID_REQUIRED_FIELD', null, {missingTranslationMsg: 'This field is required'})};
                isValidate = false;
            }
        }
        this.setState({isValid: validate,invalidTitles});
        return isValidate;
    }

    /** validation for repeat new passwords **/
    validateRepeat() {
        const { translate } = this.props;
        let isValidate = true;
        let validate = {
            current_password: true,
            new_password: true,
            repeat_password: true
        };
        let invalidTitles = {};
        if(this.state.new_password !== this.state.repeat_password) {
            validate = {
                current_password: true,
                new_password: false,
                repeat_password: false,
            };
            invalidTitles = {
                new_password: translate('PASSWORDS_DO_NOT_MATCH', null, {missingTranslationMsg: 'Passwords do not match'}),
                repeat_password: translate('PASSWORDS_DO_NOT_MATCH', null, {missingTranslationMsg: 'Passwords do not match'}),
            };
            isValidate = false;
        }
        this.setState({isValid: validate,invalidTitles});
        return isValidate;
    }

    /** inputs handler **/
    handlerChange(e,field) {
        this.setState({
            [field]:e.target.value,
            isValid: {...this.state.isValid, [field]:true}
        });
    }

    render() {
        const { translate } = this.props;
        return(
            <div className="content password-frame">
                <h2 className="reg-title">{translate('CHANGE_PASSWORD', null, {missingTranslationMsg: 'Change password'})}</h2>
                <form onSubmit={(e)=>{this.submitForm(e)}}>
                    <InputPassword id='current_password'
                                   name='current_password'
                                   value={this.state.current_password}
                                   title={translate('CURRENT_PASSWORD', null, {missingTranslationMsg: 'Current password'})}
                                   handler={(e) => this.handlerChange(e, 'current_password')}
                                   validation_title={this.state.invalidTitles.current_password}
                                   required={true}
                                   isValid={this.state.isValid.current_password}/>
                    <InputPassword id='new_password'
                                   name='new_password'
                                   value={this.state.new_password}
                                   title={translate('NEW_PASSWORD', null, {missingTranslationMsg: 'New password'})}
                                   handler={(e) => this.handlerChange(e, 'new_password')}
                                   validation_title={this.state.invalidTitles.new_password}
                                   required={true}
                                   isValid={this.state.isValid.new_password}/>
                    <InputPassword id='repeat_password'
                                   name='repeat_password'
                                   value={this.state.repeat_password}
                                   title={translate('REPEAT_NEW_PASSWORD', null, {missingTranslationMsg: 'Repeat new password'})}
                                   handler={(e) => this.handlerChange(e, 'repeat_password')}
                                   validation_title={this.state.invalidTitles.repeat_password}
                                   required={true}
                                   isValid={this.state.isValid.repeat_password}/>
                    <button className="btn-main" type="submit">{translate('APPLY', null, {missingTranslationMsg: 'Apply'})}</button>
                </form>
            </div>
        )
    }
}

Password.propTypes = {
    translate: PropTypes.func,
    notify: PropTypes.func,
    submitPassword: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        notify: (show, error, message) => {
            return dispatch(notificationAction(show, error, message))
        },
        submitPassword: (data) => {
            return dispatch(submitPasswordActionThunk(data))
        }
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Password);