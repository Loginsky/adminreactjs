import React, {Component} from 'react';
import {withRouter, Switch, Route} from 'react-router-dom';
import {connect} from "react-redux";
import classNames from 'classnames';
import LoadingBar from 'react-redux-loading-bar'

// containers
import HeaderView from './containers/HeaderView';
import LogInView from './containers/LogInView';
import NotificationView from './containers/NotificationView';
import RegisterView from './containers/RegisterView';
import ProfileView from './containers/ProfileView';

// components
import LanguageVars from './components/LanguageVars';
import Notifications from './components/Notifications';
import Contacts from './components/Contacts/Contacts';
import Password from './components/Password/Password';
import Company from './components/Company/Company';

// css
import './App.css';
import '@fortawesome/fontawesome/styles.css'

// action thunk
import {getAllLocalesActionThunk} from "./actions/siteLocalesAction";


class App extends Component {

    constructor(props) {
        super(props);

        // get all preferences
        this.props.getAllPreferencesThunk();
        // get all skills
        this.props.getAllSkillsThunk();
        // get all locales and set User locale
        this.props.getAllLocalesThunk();
    }

    render() {

        const {location} = this.props;
        return (
            <div className="App">
                <LoadingBar style={{backgroundColor: '#a8bc4d'}}/>
                <HeaderView/>
                <main className={classNames('container', {
                    'login': location.pathname === '/',
                    'congratulations': location.pathname === '/register/done'
                })}>
                    <NotificationView/>
                    <Switch>
                        <Route exact path="/" component={LogInView}/>
                        <PrivateRoute path="/language_vars" component={LanguageVars}/>
                        <PrivateRoute path="/notifications" component={Notifications}/>
                        <PrivateRoute path="/profile" component={ProfileView}/>
                        <PrivateRoute path="/user_profile/:id" component={ProfileView}/>
                        <PrivateRoute path="/password" component={Password}/>
                        <PrivateRoute path="/company" component={Company}/>
                        <Route path="/register" component={RegisterView}/>
                    </Switch>
                </main>
                <footer className='container footer'>
                    <p>© {(new Date()).getFullYear()} Developers Bay</p>
                </footer>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAllPreferencesThunk: () => {
            return dispatch(getAllPreferencesThunk())
        },
        getAllSkillsThunk: () => {
            return dispatch(getAllSkillsThunk())
        },
        getAllLocalesThunk: () => {
            return dispatch(getAllLocalesActionThunk())
        }
    }
};

export default withRouter(connect(null, mapDispatchToProps)(App));
