import SingleReport from "../components/TimeReports/SingleReport/SingleReport";

const config = {
    roles: {
        admin: {
            menu: [
                {
                    label_key: 'PREFERENCES_MENU_ITEM_TITLE',
                    def_label: 'Preferences',
                    route: '/preferences',
                    isSubMenuItem: true
                },
                {
                    label_key: 'LANG_VARS_MENU_ITEM_TITLE',
                    def_label: 'Language Variables',
                    route: '/language_vars',
                    isSubMenuItem: true
                },
                {
                    label_key: 'SETTINGS_MENU_ITEM_TITLE',
                    def_label: 'Settings',
                    route: '/site_settings',
                    isSubMenuItem: true
                },
                {
                    label_key: 'NOTIFICATIONS_MENU_ITEM_TITLE',
                    def_label: 'Notifications',
                    route: '/notifications',
                    isSubMenuItem: true
                }

            ],
            availableRoutes: [
                {label_key: 'CONTACTS_MENU_ITEM_TITLE', def_label: 'Contacts', route: '/contacts'},
                {label_key: 'SINGLE_TIME_REPORT', def_label: 'Single Time Report', route: '/time_report/:id'},
                {label_key: 'USER_PROFILE', def_label: 'User profile', route: '/user_profile/:id'},
            ],
            profile_tabs: [
                {route: '', label_key: 'HI_HEADER', def_label: 'Hi'},
                {route: '/contact_details', label_key: 'CONTACT_INFORMATION', def_label: 'Contact Information'},
            ],
            rootRoute: '/search',
            contact_details_fields: ['title', 'email', 'address', 'first_name', 'postal_code', 'last_name', 'country_code',
                'phone', 'type_id', 'status_id', 'test_result', 'priority', 'possible_customer', 'price_per_hour',
                'is_tested', 'terms_accepted'],
            user_settings_fields: ['assignmentsPreferences', 'availabilities', 'placements', 'missionLengths',
                'from_customer', 'price_level', 'branches', 'wish', 'value_project', 'avoid_project'],
            cv_fields: ['image_file', 'title_2', 'additional_description', 'capabilities', 'years', 'experiences',
                'educations', 'certifications', 'languages', 'linkedin_url', 'github_url', 'website_url'],
            legal_Information_tabs: ['company_type_id', 'tax_registered', 'vat_registered', 'name', 'organization_number',
                'signer', 'invoice_address', 'invoice_mailing_address', 'bank', 'bank_account', 'iban', 'bic', 'bankgiro',
                'other_information']
        },
        superuser: {
            menu: [
                {
                    label_key: 'SETTINGS_MENU_ITEM_TITLE',
                    def_label: 'Settings',
                    route: '/site_settings',
                    isSubMenuItem: true
                },
            ],
            availableRoutes: [],
            rootRoute: '/search',
            contact_details_fields: ['title', 'email', 'address', 'first_name', 'postal_code', 'last_name', 'country_code',
                'phone', 'type_id', 'status_id', 'is_tested', 'test_result', 'terms_accepted', 'possible_customer',
                'priority', 'price_per_hour'],
            user_settings_fields: ['assignmentsPreferences', 'availabilities', 'placements', 'missionLengths',
                'from_customer', 'price_level', 'branches', 'wish', 'value_project', 'avoid_project'],
            cv_fields: ['image_file', 'title_2', 'additional_description', 'capabilities', 'years', 'experiences',
                'educations', 'certifications', 'languages', 'linkedin_url', 'github_url', 'website_url'],
            legal_Information_tabs: ['company_type_id', 'tax_registered', 'vat_registered', 'name', 'organization_number',
                'signer', 'invoice_address', 'invoice_mailing_address', 'bank', 'bank_account', 'iban', 'bic', 'bankgiro',
                'other_information']
        },
        worker: {
            menu: [
                {label_key: 'USER_PROFILE_MENU_ITEM_TITLE', def_label: 'User Profile', route: '/profile'},
                {label_key: 'CONTACTS_MENU_ITEM_TITLE', def_label: 'Contacts', route: '/contacts'},
                {label_key: 'TERMS_MENU_ITEM_TITLE', def_label: 'Terms and Agreement', route: '/terms'},
                {label_key: 'TIME_REPORTS_MENU_ITEM_TITLE', def_label: 'Time Management', route: '/project'},
                {label_key: 'CHANGE_PASSWORD_MENU_ITEM_TITLE', def_label: 'Change Password', route: '/password'}
            ],
            rootRoute: '/profile',
            contact_details_fields: ['email', 'address', 'first_name', 'postal_code', 'last_name', 'country_code', 'phone'],
            user_settings_fields: ['assignmentsPreferences', 'availabilities', 'placements', 'missionLengths',
                'from_customer', 'price_level', 'branches', 'wish', 'value_project', 'avoid_project'],
            cv_fields: ['image_file', 'title_2', 'additional_description', 'capabilities', 'years', 'experiences',
                'educations', 'certifications', 'languages', 'linkedin_url', 'github_url', 'website_url'],
            legal_Information_tabs: ['company_type_id', 'tax_registered', 'vat_registered', 'name', 'organization_number',
                'signer', 'invoice_address', 'invoice_mailing_address', 'bank', 'bank_account', 'iban', 'bic', 'bankgiro',
                'other_information']
        },
        'default': {
            contact_details_fields: ['email', 'address', 'first_name', 'postal_code', 'last_name', 'country_code', 'phone'],
            availableRoutes: [],
            user_settings_fields: ['assignmentsPreferences', 'availabilities', 'placements', 'missionLengths',
                'from_customer', 'price_level', 'branches', 'wish', 'value_project', 'avoid_project'],
            cv_fields: ['image_file', 'title_2', 'additional_description', 'capabilities', 'years', 'experiences',
                'educations', 'certifications', 'languages', 'linkedin_url', 'github_url', 'website_url']

        }
    },
    routes: {
        register: [
            {route: '/contact_details', label_key: 'SIGN_UP_TAB_TITLE', def_label: 'Sign Up', allowed: true},
        ]
    }
};

export default config;