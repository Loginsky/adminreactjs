import axios from 'axios';
import store from './store';

const RequestAPI = axios.create({baseURL: process.env.REACT_APP_API_HOST});

RequestAPI.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('id_token') || null}`;
RequestAPI.defaults.headers.common['Content-Type'] = 'application/json';

export default RequestAPI;