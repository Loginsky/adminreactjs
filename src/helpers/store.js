import reducers from "../reducers";
import {routerMiddleware} from "react-router-redux";
import {applyMiddleware, createStore} from "redux";
import history from "./history";
import thunk from 'redux-thunk';

const logger = store => next => action => {
    // console.group(action.type);
    // console.info('dispatching', action);
    let result = next(action)
    // console.info('next state', store.getState());
    // console.groupEnd(action.type);
    return result
};

const store = createStore(reducers,applyMiddleware(thunk,logger,routerMiddleware(history)));

export default store;