import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect, withRouter } from 'react-router-dom';
import { getTranslate } from "react-localize-redux";
import PropTypes from "prop-types";

// components
import LogIn from '../components/LogIn/LogIn';

// actions
import { logInAction } from '../actions/logInAction';
import { forgotAction } from '../actions/forgotAction';
import { notificationAction } from '../actions/notificationAction';
import { setRegistrationStatusAction } from "../actions/registerAction";
import { resetRegistrationRoutesAction } from "../actions/registerAction";

// config
import config from '../config/config'

class LogInView extends Component {
    constructor(...props) {
        super(...props);

        this.state = {
            email:'',
            password:'',
            remember:true,
            emailForgot:'',
            showForgot:false,
            redirectToReferrer: false,
            redirectToHome: false,
            isValid: {
                email:true,
                password:true,
                emailForgot:true
            },
            isValidLogin: true,
            isValidForgot: true
        };

        this.submitLogin = this.submitLogin.bind(this);
        this.submitForgot = this.submitForgot.bind(this);
        this.handlerChange = this.handlerChange.bind(this);
        this.handlerCheckChange = this.handlerCheckChange.bind(this);
        this.toggleForgot = this.toggleForgot.bind(this);
        this.clickRegister = this.clickRegister.bind(this);
    }

    componentDidMount() {
        this.setState({redirectToReferrer:this.props.isAuthenticated});
        // reset register progress if user goes back to login page
        if (this.props.registerStatus) {
            this.props.setRegisterStatus(false);
            this.props.resetRegistrationRoutes();
        }
    }

    toggleForgot(e) {
        e.preventDefault();
        this.setState({showForgot:!this.state.showForgot});
    }

    submitLogin(e,fields) {
        e.preventDefault();
        this.validateFields(fields).then((isValid)=>{
            if(!isValid) {
                return;
            }
            this.props.logIn(this.state.email,this.state.password,this.state.remember).then(result => {
                console.info(result);
                this.setState({redirectToReferrer:true});
            },error=>{
                const { translate } = this.props;
                console.error(error);
                this.showNotify(true,true,translate('INCORRECT_CREDENTIALS', null, {missingTranslationMsg: 'Incorrect credentials'}));
            })
        });
    }

    submitForgot(e,fields) {
        e.preventDefault();
        this.validateFields(fields).then((isValid)=>{
            if(!isValid) {
                return;
            }
            this.props.forgotPassword(this.state.emailForgot).then((result) => {
                if(result.success) {
                    this.setState({emailForgot:'',showForgot:false});
                }
                this.showNotify(true,!result.success,result.message);
            });
        })
    }

    showNotify(show,error,message) {
        this.props.notify(show,error,message);
        setTimeout(()=>{
            this.props.notify();
        },5000);
    }

    handlerChange(e,field) {
        const { value } = e.target;
        this.setState({
            [field]:value,
            isValid: Object.assign({},this.state.isValid, {[field]:true})
        });
    }

    handlerCheckChange(e) {
        this.setState({remember:e.target.checked});
    }

    validateFields(fields) {
        return new Promise((reject) => {
            let validate = {};
            fields.forEach((field) => {
                switch (field) {
                    case 'email':
                        validate = Object.assign({}, validate, {[field]: this.state[field].toString().trim().length > 0});
                        break;
                    case 'password':
                        validate = Object.assign({}, validate, {[field]: this.state[field].toString().trim().length > 0});
                        break;
                    case 'emailForgot':
                        validate = Object.assign({}, validate, {[field]: this.state[field].toString().trim().length > 0});
                        break;
                    default:
                        break;
                }
            });

            this.setState({isValid: Object.assign({}, this.state.isValid, validate)}, () => {
                let isValidForm = true;
                fields.forEach((field) => {
                    if (!this.state.isValid[field]) {
                        isValidForm = false;
                    }
                });
                reject(isValidForm);
            });
        })
    }

    clickRegister() {
        this.props.setRegisterStatus(true);
    }

    render() {
        const { role } = this.props;
        let rootRoute = '/';
        if(this.props.isAuthenticated) {
            rootRoute = config.roles[role].rootRoute;
        }
        const { from } = this.props.location.state || { from: { pathname: rootRoute } };
        const { redirectToReferrer } = this.state;
        const logInAttr = {
            email:this.state.email,
            password:this.state.password,
            remember:this.state.remember,
            showForgot:this.state.showForgot,
            isValid: this.state.isValid,
            toggleForgot:this.toggleForgot,
            submitLogin:this.submitLogin,
            submitForgot:this.submitForgot,
            handlerChange:this.handlerChange,
            handlerCheckChange:this.handlerCheckChange,
            clickRegister:this.clickRegister,
            translate:this.props.translate
        };
        if (redirectToReferrer) {
            return <Redirect
                to={from}
            />;
        }
        return (
            <LogIn {...logInAttr}/>
        )
    }
}

LogInView.propTypes = {
    translate:PropTypes.func,
    isAuthenticated:PropTypes.bool,
    logIn:PropTypes.func,
    forgotPassword:PropTypes.func,
    notify:PropTypes.func,
    role:PropTypes.string,
    setRegisterStatus:PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
        isAuthenticated: state.authentication.isAuthenticated,
        role: state.authentication.role,
        registerStatus: state.register.status
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logIn: (email,password,remember) => {
            return dispatch(logInAction(email,password,remember))
        },
        forgotPassword: (email) => {
            return dispatch(forgotAction(email))
        },
        notify: (show,error,message) => {
            return dispatch(notificationAction(show,error,message))
        },
        setRegisterStatus: (status) => {
            return dispatch(setRegistrationStatusAction(status))
        },
        resetRegistrationRoutes: () => {
            return dispatch(resetRegistrationRoutesAction())
        }
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps) (LogInView));