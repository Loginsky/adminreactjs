import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from "prop-types";

// components
import Notification from '../components/Notification/Notification';

class NotificationView extends Component {
    render() {
        const { show, error, message } = this.props;
        if(show) {
            return(
                <Notification error={error} message={message}/>
            )
        } else {
            return null;
        }
    }
}

NotificationView.propTypes = {
    show: PropTypes.bool,
    error: PropTypes.bool,
    message: PropTypes.string
};

const mapStateToProps = (state) => {
    return {
        show: state.notification.show,
        error: state.notification.error,
        message: state.notification.message
    }
};

export default connect(mapStateToProps) (NotificationView);