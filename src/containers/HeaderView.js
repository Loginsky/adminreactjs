import React, {Component} from 'react';
import {connect} from "react-redux";
import {withRouter, Link} from 'react-router-dom';
import PropTypes from "prop-types";
import {getTranslate} from 'react-localize-redux';

// components
import Header from '../components/Header/Header';
import MenuLink from '../components/Links/MenuLink';

// actions
import {logOutAction} from "../actions/logOutAction";

// config
import config from '../config/config';

// icons
import Icons from '../icons';

class HeaderView extends Component {
    constructor(...props) {
        super(...props);

        this.state = {
            subMenuOpen: false
        }

        this.logOut = this.logOut.bind(this);
        this.toggleSubMenu = this.toggleSubMenu.bind(this);
    }

    logOut(e) {
        e.preventDefault();
        this.props.logOut().then((result) => {

        });
    }

    toggleSubMenu(e) {
        e.preventDefault();
        this.setState({subMenuOpen: !this.state.subMenuOpen});
    }

    getMenuItems() {
        if (this.props.isAuthenticated) {
            return (
                <ul>
                    {(config.roles[this.props.role].menu.filter(item => !item.isSubMenuItem)).map((item, key) => {
                        return <MenuLink key={key} to={item.route}
                                         label={this.props.translate(item.label_key, null, {missingTranslationMsg: item.def_label})}/>
                    })}
                    {(config.roles[this.props.role].menu.some(item => item.isSubMenuItem)) &&
                    <li><a onClick={(e) => {
                        this.toggleSubMenu(e)
                    }}>{this.props.translate('ADMIN_MENU_ITEM_TITLE', null, {missingTranslationMsg: 'Admin'})}</a></li>
                    }
                    <li><a onClick={(e) => {
                        this.logOut(e)
                    }}>{this.props.translate('LOGOUT_MENU_ITEM_TITLE', null, {missingTranslationMsg: 'Log Out'})}</a>
                    </li>
                </ul>
            )
        }
    }

    getSubMenuItems() {
        if (this.props.isAuthenticated) {
            return (
                <ul>
                    {(config.roles[this.props.role].menu.filter(item => item.isSubMenuItem)).map((item, key) => {
                        return <MenuLink key={key} to={item.route}
                                         label={this.props.translate(item.label_key, null, {missingTranslationMsg: item.def_label})}/>
                    })}
                </ul>
            )
        }
    }

    getLogo() {
        const to = this.props.isAuthenticated ? config.roles[this.props.role].rootRoute : '/';
        return (
            <div className='logo'>
                <Link to={to}>
                    <img src={Icons.logo} alt="Logo"/>
                </Link>
            </div>
        )
    }

    render() {
        const attr = {
            translate: this.props.translate,
            logo: this.getLogo(),
            menuItems: this.getMenuItems(),
            subMenuOpen: this.state.subMenuOpen,
            subMenuItems: this.getSubMenuItems()
        };
        return (
            <Header {...attr}/>
        )
    }
}

HeaderView.propTypes = {
    isAuthenticated: PropTypes.bool,
    role: PropTypes.string,
    translate: PropTypes.func,
    logOut: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
        isAuthenticated: state.authentication.isAuthenticated,
        role: state.authentication.role
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {
            return dispatch(logOutAction())
        },
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderView));