import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {withRouter, Switch, Route} from "react-router-dom";
import {getTranslate} from "react-localize-redux";
import _ from 'lodash';

// components
import ContentMenu from '../components/ContentMenu/ContentMenu';
import TabLink from '../components/Links/TabLink';

// containers
import StartPageView from './UserProfile/StartPageView';
import ContactDetailsView from './UserProfile/ContactDetailsView';
import UserSkillsView from "./UserProfile/UserSkillsView";
import CVView from "./UserProfile/CVView";
import UserSettingsView from "./UserProfile/UserSettingsView";
import SkillsLevelView from "./UserProfile/SkillsLevelView";
import LegalInformationView from "./UserProfile/LegalInformationView";

// config
import config from '../config/config'

// actions
import { selectEditingUserAction } from '../actions/userProfileAction';

class ProfileView extends Component {
    constructor(...props) {
        super(...props);

        // set selected user id in store
        if(_.has(this.props.match.params,'id') && this.props.match.params.id) {
            this.props.selectEditingUser(this.props.match.params.id)
        } else {
            this.props.selectEditingUser(this.props.auth_id)
        }
    }

    getTabs() {
        const routes = config.roles[this.props.role].profile_tabs;
        const {match} = this.props;
        return (
            <ul className="register-tabs">
                {routes.map((item, index) => {
                    return <TabLink
                        key={index}
                        to={match.url + item.route}
                        label={this.props.translate(item.label_key, null, {missingTranslationMsg: item.def_label})}
                        activeOnlyWhenExact={true}/>
                })}
            </ul>
        )
    }

    componentWillUnmount() {
        // reset selected user id store
        this.props.selectEditingUser(null);
    }

    render() {
        const Fragment = React.Fragment;
        const {match, location} = this.props;
        return (
            <Fragment>
                <ContentMenu menuTabs={this.getTabs()}/>
                <div className="content">
                    {location.pathname === match.url && <Route path={`${match.url}`} component={StartPageView}/>}
                    <Switch>
                        <Route path={`${match.url}/contact_details`} component={ContactDetailsView}/>
                        <Route path={`${match.url}/settings`} component={UserSettingsView}/>
                        <Route path={`${match.url}/skills`} component={UserSkillsView}/>
                        <Route path={`${match.url}/skills_level`} component={SkillsLevelView}/>
                        <Route path={`${match.url}/cv`} component={CVView}/>
                        <Route path={`${match.url}/legal_information`} component={LegalInformationView}/>
                    </Switch>
                </div>
            </Fragment>
        )
    }
}

ProfileView.propTypes = {
    translate: PropTypes.func,
    role: PropTypes.string,
    auth_id: PropTypes.number,
    selectEditingUser: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
        role: state.authentication.role,
        auth_id: parseInt(state.authentication.id,10)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectEditingUser:(id) => {
            return dispatch(selectEditingUserAction(id))
        }
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(ProfileView));