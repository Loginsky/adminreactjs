import React, { Component } from 'react';
import { withRouter, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import { getTranslate } from 'react-localize-redux';
import PropTypes from "prop-types";

// components
import ContentMenu from '../components/ContentMenu/ContentMenu';
import RegisterDone from '../components/RegisterDone/RegisterDone';
import RegisterRoute from '../components/RegisterRoute';
import TabLinkRegister from '../components/Links/TabLinkRegister';
import ConfirmRegister from '../components/ConfirmRegister/ConfirmRegister';

// containers
import ContactDetailsView from './UserProfile/ContactDetailsView';
import UserSettingsView from './UserProfile/UserSettingsView';

class RegisterView extends Component {
    getTabs() {
        const { match, routes } = this.props;
        if(this.props.registerStatus) {
            return(
                <ul className="register-tabs">
                    {routes.map((item,index)=>{
                        return <TabLinkRegister key={index} to={match.url + item.route} allowed={item.allowed} label={this.props.translate(item.label_key, null, {missingTranslationMsg: item.def_label})}/>
                    })}
                </ul>
            )
        } else {
            return null;
        }
    }

    render() {
        const Fragment = React.Fragment;
        const { match, location } = this.props;
        if(location.pathname === match.path) {
            return (<Redirect to={`${match.url}/contact_details`}/>)
        }
        return (
            <Fragment>
                <ContentMenu menuTabs={this.getTabs()}/>
                <div className="content">
                    <Switch>
                        <RegisterRoute path={`${match.url}/contact_details`} component={ContactDetailsView} />
                        <RegisterRoute path={`${match.url}/settings`} component={UserSettingsView} />
                    </Switch>
                    <Route path={`${match.url}/done`} component={RegisterDone} />
                    <Route path={`${match.url}/confirm`} component={ConfirmRegister} />
                </div>
            </Fragment>
        )
    }
}

RegisterView.propTypes = {
    translate:PropTypes.func,
    routes:PropTypes.array,
    setRegisterStatus:PropTypes.func,
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
        routes: state.register.routes,
        registerStatus: state.register.status
    }
};

export default withRouter(connect(mapStateToProps) (RegisterView));