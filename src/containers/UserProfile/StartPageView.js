import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getTranslate } from "react-localize-redux";
import moment from 'moment';

// components
import StartProfilePage from '../../components/StartProfilePage/StartProfilePage'

// actions
import { notificationAction } from "../../actions/notificationAction";

// action thunk
import { addStartCommentThunk } from '../../actions/userProfileAction'
import { getStartTabInfoActionThunk } from "../../actions/userProfileAction";

class StartPageView extends Component {

    constructor(...props) {
        super(...props);

        this.state = {
            availability_date: moment(),
            availability_comment: '',
            switch_earlier: false,
            isValid: {
                availability_comment: true
            },
            heading : '',
            main_text : '',
            sub_text : ''
        };

        this.submitForm = this.submitForm.bind(this);
        this.handlerChangeComment = this.handlerChangeComment.bind(this);
        this.handlerChangeDate = this.handlerChangeDate.bind(this);
        this.handlerChangeCheck = this.handlerChangeCheck.bind(this);

        this.props.getStartTabInfo(this.props.user_id).then(result => {
            this.setState({
                availability_date: result.data.availability_date ? moment(result.data.availability_date) : moment(),
                availability_comment: result.data.availability_comment || '' ,
                switch_earlier: !!result.data.switch_earlier,
                heading : result.data.heading || "Test heading edited",
                main_text : result.data.main_text || "Test main text edited",
                sub_text : result.data.sub_text ||  "Text sub text edited"
            });
        });
    }

    /** submit **/
    submitForm(e) {
        e.preventDefault();
        if(this.validateFields()) {
            const data = {
                availability_date: this.state.availability_date.format('YYYY-MM-DD'),
                availability_comment: this.state.availability_comment,
                switch_earlier: this.state.switch_earlier
            };
            this.props.addStartComment(data,this.props.user_id)
                .then(result => {
                    console.info(result);
                    this.showNotify(true, false, this.props.translate('LOG_HAS_BEEN_SAVED', null, {missingTranslationMsg: 'The log has been saved'}));
                },error => {
                    console.warn(error);
                    this.showNotify(true, true, this.props.translate('BAD_SERVER_REQUEST', null, {missingTranslationMsg: 'Sorry... bad server request...'}));
                });
        }
    }

    /** validation **/
    validateFields() {
        let isValidate = true;
        let validate = {};
        for(let key in this.state.isValid) {
            validate = {...validate, [key]:this.state[key].toString().trim().length > 0};
            if(!this.state[key].toString().trim().length > 0) {
                isValidate = false;
            }
        }
        this.setState({isValid: validate});
        return isValidate;
    }

    /** notification **/
    showNotify(show,error,message) {
        this.props.notify(show,error,message);
        setTimeout(()=>{
            this.props.notify();
        },5000);
    }

    /** handlers **/
    handlerChangeComment(e) {
        this.setState({
            availability_comment:e.target.value,
            isValid: {...this.state.isValid, availability_comment:true}
        })
    }

    handlerChangeDate(availability_date) {
        this.setState({availability_date});
    }

    handlerChangeCheck(e) {
        this.setState({switch_earlier:e.target.checked});
    }

    render() {
        const attr = {
            translate: this.props.translate,
            submitForm: this.submitForm,
            availability_date: this.state.availability_date,
            availability_comment: this.state.availability_comment,
            switch_earlier: this.state.switch_earlier,
            handlerChangeComment: this.handlerChangeComment,
            handlerChangeDate: this.handlerChangeDate,
            handlerChangeCheck: this.handlerChangeCheck,
            isValid: this.state.isValid,
            heading : this.state.heading,
            main_text : this.state.main_text,
            sub_text : this.state.sub_text
        };
        return(
            <StartProfilePage {...attr}/>
        )
    }
}

StartPageView.propTypes = {
    translate: PropTypes.func,
    user_id: PropTypes.number,
    getStartTabInfo: PropTypes.func,
    addStartComment: PropTypes.func,
    notify: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
        user_id: parseInt(state.editingUser.selectedUser,10)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getStartTabInfo: (id) => {
            return dispatch(getStartTabInfoActionThunk(id))
        },
        addStartComment: (data,id) => {
            return dispatch(addStartCommentThunk(data,id))
        },
        notify: (show,error,message) => {
            return dispatch(notificationAction(show,error,message))
        },
    }
};

export default connect(mapStateToProps,mapDispatchToProps) (StartPageView);