import React, {Component} from 'react';
import PropTypes from "prop-types";
import _ from 'lodash';
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { getTranslate } from "react-localize-redux";

// actions
import {checkEmailWhenRegister, setRegisterUserContactsAction} from "../../actions/registerAction";
import {notificationAction} from "../../actions/notificationAction";

// action thunk
import { getUserContactDetailsActionThunk,
         submitUserContactsThunk,
         getUserTypesStatusesActionThunk} from "../../actions/userProfileAction";

// components
import ContactDetails from '../../components/ContactDetails/ContactDetails';

class ContactDetailsView extends Component {
    constructor(...props) {
        super(...props);

        this.state = {
            contacts: {},
            userTypes:[],
            user_types: [],
            user_statuses: [],
            isValid: {
                email: true,
                first_name: true,
                last_name: true,
                phone: true,
                country_code: true
            }
        };

        // define next route for register
        this.nextRoute = '/settings';

        this.submitForm = this.submitForm.bind(this);
        this.handlerChange = this.handlerChange.bind(this);
        this.handlerCheckboxChange = this.handlerCheckboxChange.bind(this);
        this.handleChangePriority = this.handleChangePriority.bind(this);

        this.changeUserType = this.changeUserType.bind(this);
        this.changeUserStatus = this.changeUserStatus.bind(this);
    }

    /** init and set state **/
    componentWillMount() {
        if (this.props.isRegisterMode) {
            const contacts = this.props.registerContacts;
            this.setState({contacts});
        } else {
            const contacts = {
                title: '',
                email: '',
                first_name: '',
                last_name: '',
                phone: '',
                address: '',
                postal_code: '',
                country_code: '',
                is_tested: true,
                test_result: '',
                terms_accepted: true,
                possible_customer: '',
                priority: 0,
                price_per_hour: '',
                type_id: null, // ask default value
                status_id: null // ask default value
            };
            this.setState({contacts});
            this.props.getUserTypesStatuses(this.props.user_id).then(result=>{
                this.setState({userTypes:result},()=>{
                    this.setUserTypeSelects(this.state.userTypes);
                });
            });
            this.props.getUserProfileContactDetails(this.props.user_id).then(result => {
                let contacts = result;
                // ToDo: test. Remove after update back-end
                contacts.type_id = 2;
                contacts.status_id = 21;

                this.setState({contacts});
                this.setUserTypeSelects(this.state.userTypes);
            })
        }
    }

    setUserTypeSelects(userTypes) {
        if(userTypes.length) {
            const user_types = userTypes.map(type=>{
                return {
                    value: type.id,
                    label: type.name
                }
            });
            this.setState({user_types},()=>{
                this.setUserStatusesSelect(userTypes, this.state.contacts.type_id)
            });
        }
    }

    setUserStatusesSelect(userTypes,type_id,selectFirst) {
        const userTypeArr = userTypes.filter(type => type.id === type_id);
        const userTypeObj = userTypeArr.length ? userTypeArr[0] : {};
        if(_.has(userTypeObj,'statuses')) {
            const user_statuses = userTypeObj.statuses.map(status=>{
                return {
                    value: status.id,
                    label: status.name
                }
            });
            this.setState({user_statuses},()=>{
                if(selectFirst) {
                    const status_id = this.state.user_statuses.length ? this.state.user_statuses[0].value : null;
                    this.setState({contacts: {...this.state.contacts, status_id}})
                }
            });
        }
    }

    /** main submit **/
    submitForm(e) {
        e.preventDefault();
        if (this.validateFields()) {
            const {contacts} = this.state;
            if (this.props.isRegisterMode) {
                // if this is registration
                this.props.checkEmail({email: contacts.email}, this.nextRoute).then(result => {
                    this.props.setRegisterContacts(contacts);
                    const { history } = this.props;
                    history.push('/register' + this.nextRoute);
                }, error => {
                    console.warn(error);
                    this.showNotify(true, true, this.props.translate('BAD_SERVER_REQUEST', null, {missingTranslationMsg: 'Sorry... but user with this email is already exist'}));
                });
            } else {
                // if this is edit user
                console.log(this.state.contacts);
                console.log(this.state.userTypes);
                this.props.submitUserContacts({contacts: this.state.contacts},this.props.user_id)
                    .then(result => {
                        console.info(result);
                        this.showNotify(true, false, this.props.translate('CONTACTS_HAS_BEEN_SAVED', null, {missingTranslationMsg: 'User contacts has been saved'}));
                    }, error => {
                        console.warn(error);
                        this.showNotify(true, true, this.props.translate('BAD_SERVER_REQUEST', null, {missingTranslationMsg: 'Sorry... bad server request...'}));
                    });
            }
        }
    }

    /** notifications **/
    showNotify(show, error, message) {
        this.props.notify(show, error, message);
        setTimeout(() => {
            this.props.notify();
        }, 5000);
    }

    /** validation **/
    validateFields() {
        let isValidate = true;
        let validate = {};
        for (let key in this.state.isValid) {
            if(key === 'phone') {
                let phoneValidation = this.state.contacts[key].toString().trim().length > 0 && this.validatePhone(this.state.contacts[key]);
                validate = {...validate, [key]: phoneValidation};
                if (!phoneValidation) {
                    isValidate = false;
                }
               this.validatePhone(this.state.contacts[key]);
            } else {
                validate = {...validate, [key]: this.state.contacts[key].toString().trim().length > 0};
                if (!this.state.contacts[key].toString().trim().length > 0) {
                    isValidate = false;
                }
            }
        }
        this.setState({isValid: validate});
        return isValidate;
    }

    /** phone number validation **/
    validatePhone(phone) {
        // 0970996699
        const rex1 = /^[0-9]{10}$/;
        // +380970996699
        const rex2 = /^\+[0-9]{12}$/;
        // +38(097)0996699
        const rex3 = /^\+[0-9]{2}\([0-9]{3}\)[0-9]{7}$/;
        // +38(097)099-66-99
        const rex4 = /^\+[0-9]{2}\([0-9]{3}\)[0-9]{3}[-][0-9]{2}[-][0-9]{2}$/;
        return (phone.match(rex1) || phone.match(rex2) || phone.match(rex3) || phone.match(rex4));
    }

    /** change handler **/
    handlerChange(e, field) {
        const {value} = e.target;
        this.setState({
            contacts: {...this.state.contacts, [field]: value},
            isValid: {...this.state.isValid, [field]: true}
        });
    }

    /** change checkbox handler **/
    handlerCheckboxChange(e, field) {
        const {checked} = e.target;
        this.setState({
            contacts: {...this.state.contacts, [field]: checked}
        });
    }

    /** change usert priority **/
    handleChangePriority(value) {
        this.setState({
            contacts: {...this.state.contacts, priority: value}
        });
    }

    /** change user type (select) **/
    changeUserType(e) {
        this.setState({contacts: {...this.state.contacts, type_id:parseInt(e.value,10)}},()=>{
            // update user statuses select
            this.setUserStatusesSelect(this.state.userTypes, this.state.contacts.type_id, true);
        })
    }

    /** change user status (select) **/
    changeUserStatus(e) {
        this.setState({contacts: {...this.state.contacts, status_id:parseInt(e.value,10)}})
    }

    render() {
        const attr = {
            contacts: this.state.contacts,
            isValid: this.state.isValid,
            handlerChange: this.handlerChange,
            handlerCheckboxChange: this.handlerCheckboxChange,
            handleChangePriority: this.handleChangePriority,
            submitForm: this.submitForm,
            translate: this.props.translate,
            role: this.props.role,
            isRegisterMode: this.props.isRegisterMode,
            user_types: this.state.user_types,
            user_statuses: this.state.user_statuses,
            changeUserType: this.changeUserType,
            changeUserStatus: this.changeUserStatus
        };
        return (
            <ContactDetails {...attr}/>
        )
    }
}

ContactDetailsView.propTypes = {
    translate: PropTypes.func,
    role: PropTypes.string,
    registerContacts: PropTypes.object,
    isRegisterMode: PropTypes.bool,
    user_id: PropTypes.number,
    checkEmail: PropTypes.func,
    setRegisterContacts: PropTypes.func,
    submitUserContacts: PropTypes.func,
    getUserProfileContactDetails: PropTypes.func,
    notify: PropTypes.func,
};

const mapStateToProps = (state) => {
    return {
        translate: getTranslate(state.locale),
        role: state.authentication.role,
        registerContacts: state.registerUser.contacts,
        isRegisterMode: state.register.status,
        user_id: parseInt(state.editingUser.selectedUser,10),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        checkEmail: (email, nextRoute) => {
            return dispatch(checkEmailWhenRegister(email, nextRoute))
        },
        setRegisterContacts: (data) => {
            return dispatch(setRegisterUserContactsAction(data))
        },
        submitUserContacts: (data,id) => {
            return dispatch(submitUserContactsThunk(data,id))
        },
        getUserTypesStatuses: (id) => {
            return dispatch(getUserTypesStatusesActionThunk(id));
        },
        getUserProfileContactDetails: (id) => {
            return dispatch(getUserContactDetailsActionThunk(id));
        },
        notify: (show, error, message) => {
            return dispatch(notificationAction(show, error, message))
        }
    }
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps) (ContactDetailsView));